import { Component } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ActivityTypeRestServiceService} from '../rest/activity-type-rest-service.service'
import {SimulationRestServiceService} from '../rest/simulation-rest-service.service'
import {ConcurrentRestServiceService} from '../rest/concurrent-rest-service.service'
import {MatDialog} from '@angular/material';
import {MatSnackBar, MatSnackBarConfig,  MatSnackBarHorizontalPosition,  MatSnackBarVerticalPosition} from '@angular/material';
import { saveAs } from 'file-saver';

import { ActivityType } from '../model/activity-type';
import { Concurrent } from '../model/concurrent';
import { Simulation } from '../model/simulation';
import { FinishSimulationComponent } from '../finish-simulation/finish-simulation.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  {
  activityList : ActivityType[];
  concurrentList : Concurrent[];
  activityType = new FormControl('', Validators.required);
  concurrent = new FormControl('', Validators.required);
  numberOfIdentification = new FormControl('', Validators.required);
  phone = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);
  debitRateConcurrent = new FormControl('', Validators.required);
  creditRateConcurrent = new FormControl('', Validators.required);
  creditDiscount = new FormControl('', Validators.required);
  debitDiscount = new FormControl('', Validators.required);
  simulationStatus =new FormControl('', Validators.required);

  config = new MatSnackBarConfig();

  constructor(private activityTypeRestServiceService : ActivityTypeRestServiceService,
                private simulationRestServiceService : SimulationRestServiceService,
                  private concurrentRestService : ConcurrentRestServiceService, 
                    private snackBar: MatSnackBar,
                    private dialog: MatDialog) {
     this.setConfig();
     this.getActivityList();
     this.getConcurrentList();

   }
  setConfig(){
    let horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    let verticalPosition: MatSnackBarVerticalPosition = 'bottom';
    let autoHide: number = 10000;
    this.config.verticalPosition = verticalPosition;
    this.config.horizontalPosition = horizontalPosition
    this.config.duration = autoHide;
    this.config.panelClass = ['snackBar'] ;
  }
  getErrorConcurrentMessage() {
    return this.concurrent.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorActivityTypeMessage(){
    return this.activityType.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorNumberOfIdentificationMessage() {
    return this.numberOfIdentification.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorPhoneMessage() {
    return this.phone.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorEmailMessage(){
    return this.phone.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorDebitRateConcurrentMessage(){
    return this.debitRateConcurrent.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorDebitDiscountMessage(){
    return this.debitDiscount.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorCreditRateConcurrentMessage(){
    return this.creditRateConcurrent.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorCreditDiscountMessage(){
    return this.creditDiscount.hasError('required') ? 'Campo obrigatório' : '';
  }

  getActivityList(){
    this.activityTypeRestServiceService.getAllActivityType().subscribe(
      data => {
        this.activityList = data;
      return;
    },
    error => {
      this.snackBar.open( "Falha ao carregar lista de ramo de atividade.", "x", this.config);
      return;
    }
   );
  }
  getConcurrentList(){
    this.concurrentRestService.getAllConcurrent().subscribe(
      data => {
        this.concurrentList = data;
      return;
    },
    error => {
      this.snackBar.open( "Falha ao carregar lista de concorrentes.", "x", this.config);
      return;
    }
   );
  }
  save(){
    if(!this.activityType.errors && !this.concurrent.errors  && !this.numberOfIdentification.errors 
       && !this.phone.errors  && !this.email.errors  && !this.debitRateConcurrent.errors  
        && !this.creditRateConcurrent.errors && !this.creditDiscount.errors  && !this.debitDiscount.errors){
          let simulation : Simulation = new Simulation();
          simulation.activityType = this.activityType.value;
          simulation.concurrent = this.concurrent.value;
          simulation.numberOfIdentification = this.numberOfIdentification.value;
          simulation.creditDiscount = this.creditDiscount.value;
          simulation.creditRateConcurrent = this.creditRateConcurrent.value;
          simulation.debitDiscount = this.debitDiscount.value;
          simulation.debitRateConcurrent = this.debitRateConcurrent.value;
          simulation.email = this.email.value;
          simulation.phone = this.phone.value;
          this.simulationRestServiceService.registerSimulation(simulation).subscribe(
            data => {
              console.log(data);
              console.log(data[0]);
              if (data[0]==="Simulação permitida."){
                simulation.id = Number( data[1]);
                this.confirmSimulation(simulation);
              }else{
                this.snackBar.open( data.toString(), "X",this.config);
              }
            return;
           },
           error => {
             this.snackBar.open( "Falha ao cadastrar simulação.", "", this.config);
             return;
           }
         );

    }
  }
  confirmSimulation( simulation : Simulation){
    const dialogRef = this.dialog.open(FinishSimulationComponent,{
        height: '350px',
        width: '550px',
      });
  
    dialogRef.afterClosed().subscribe(result => {
      if(result !== '' && result !== undefined){
        simulation.simulationStatus=result;
        this.simulationRestServiceService.finishSimulation(simulation).subscribe(
          data => {
            this.snackBar.open( data.toString(), "X",this.config);
            this.clearField();
            return;
          },
          error => {
            this.snackBar.open( error.toString, "", this.config);
           return;
          }
        );
      }
    });
  }
  clearField(){
    this.activityType.reset();
    this.concurrent.reset();
    this.numberOfIdentification.reset();
    this.phone.reset();
    this.email.reset(); 
    this.debitRateConcurrent.reset();
    this.creditRateConcurrent.reset(); 
    this.creditDiscount.reset(); 
    this.debitDiscount.reset();
  }
  cancel(){
    this.clearField();
  }
   download(){
      this.simulationRestServiceService.downloadSimulations().subscribe(resultBlob => 
        {
          var blob = new Blob([resultBlob], { type: "application/octet-stream" } );
          saveAs(blob, "simulation.csv");
        },
      error => {
        console.log(error);
      });

  }
 
}
