import { ActivityType } from './activity-type';

export class Rate {
    rateType : string;
    activityType : ActivityType;
    rate : number;
    
}
