import { Concurrent } from './concurrent';
import { ActivityType } from './activity-type';

export class Simulation {
    id : number;
    concurrent :Concurrent;
    numberOfIdentification: string;
    phone : number;
    email : string;
    activityType : ActivityType;
    debitRateConcurrent : number;
    debitDiscount : number;
    creditRateConcurrent : number;
    creditDiscount : number;
    simulationStatus: string;

}
