import { Component,Inject } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-finish-simulation',
  templateUrl: './finish-simulation.component.html',
  styleUrls: ['./finish-simulation.component.css']
})
export class FinishSimulationComponent  {

  status = new FormControl('', Validators.required);
  selected = 'Recusada';
  constructor(private dialogRef: MatDialogRef<FinishSimulationComponent>) {
   }
   getErrorStatusMessage() {
    return this.status.hasError('required') ? 'Campo obrigatório' : '';
  }
   save(){
     if(!this.status.errors){
      this.dialogRef.close(this.status.value);
     }

   }

}
