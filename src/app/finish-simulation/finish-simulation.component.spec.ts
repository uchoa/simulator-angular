import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishSimulationComponent } from './finish-simulation.component';

describe('FinishSimulationComponent', () => {
  let component: FinishSimulationComponent;
  let fixture: ComponentFixture<FinishSimulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishSimulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishSimulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
