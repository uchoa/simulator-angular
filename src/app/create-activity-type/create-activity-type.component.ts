import { Component } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ActivityTypeRestServiceService} from '../rest/activity-type-rest-service.service'
import {MatSnackBar, MatSnackBarConfig,  MatSnackBarHorizontalPosition,  MatSnackBarVerticalPosition} from '@angular/material';
import { ActivityType } from '../model/activity-type';
@Component({
  selector: 'app-create-activity-type',
  templateUrl: './create-activity-type.component.html',
  styleUrls: ['./create-activity-type.component.css']
})
export class CreateActivityTypeComponent  {

  description = new FormControl('', Validators.required);
  config = new MatSnackBarConfig();

  constructor(private activityTypeRestServiceService : ActivityTypeRestServiceService, private snackBar: MatSnackBar) {
    this.setConfig();
  }
  getErrorMessage() {
    return this.description.hasError('required') ? 'Campo obrigatório' : '';
  }
  setConfig(){
    let horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    let verticalPosition: MatSnackBarVerticalPosition = 'top';
    let autoHide: number = 2000;
    this.config.verticalPosition = verticalPosition;
    this.config.horizontalPosition = horizontalPosition
    this.config.duration = autoHide;
    this.config.panelClass = ['snackBar'] ;
  }
  save(){
    if(!this.description.errors){
      let activityType : ActivityType = new ActivityType();
      activityType.description= this.description.value;
      
      this.activityTypeRestServiceService.registerActivityType(activityType).subscribe(
         data => {
            this.snackBar.open( "Ramo de atividade cadastrado com sucesso!", "x", this.config);
            this.description.reset();
          return;
        },
        error => {
          this.snackBar.open( "Falha ao cadastrar ramo de atividade.", "x", this.config);
          return;
        }
      );
    }
  }

}
