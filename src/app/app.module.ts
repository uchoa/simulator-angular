import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { CreateConcurrentComponent } from './create-concurrent/create-concurrent.component';
import { CreateActivityTypeComponent } from './create-activity-type/create-activity-type.component';
import { CreateRateComponent } from './create-rate/create-rate.component';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material';
import {MatOptionModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule , ReactiveFormsModule } from '@angular/forms'
import {MatDialogModule} from '@angular/material/dialog';
import { FinishSimulationComponent } from './finish-simulation/finish-simulation.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateConcurrentComponent,
    CreateActivityTypeComponent,
    CreateRateComponent,
    FinishSimulationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  entryComponents: [
    FinishSimulationComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
