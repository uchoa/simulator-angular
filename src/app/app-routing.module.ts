import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreateConcurrentComponent } from './create-concurrent/create-concurrent.component';
import { CreateRateComponent } from './create-rate/create-rate.component';
import { CreateActivityTypeComponent } from './create-activity-type/create-activity-type.component';

const routes: Routes = [
  {path: '', component: HomeComponent  , pathMatch: 'full'},
  {path: 'concurrent', component: CreateConcurrentComponent  },
  {path: 'rate', component: CreateRateComponent  },
  {path: 'activityType', component: CreateActivityTypeComponent  },
  {path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
