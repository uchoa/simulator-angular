import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateConcurrentComponent } from './create-concurrent.component';

describe('CreateConcurrentComponent', () => {
  let component: CreateConcurrentComponent;
  let fixture: ComponentFixture<CreateConcurrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateConcurrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateConcurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
