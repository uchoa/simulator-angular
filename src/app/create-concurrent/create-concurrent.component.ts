import { Component } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ConcurrentRestServiceService} from '../rest/concurrent-rest-service.service'
import {MatSnackBar, MatSnackBarConfig,  MatSnackBarHorizontalPosition,  MatSnackBarVerticalPosition} from '@angular/material';
import { Concurrent } from '../model/concurrent';

@Component({
  selector: 'app-create-concurrent',
  templateUrl: './create-concurrent.component.html',
  styleUrls: ['./create-concurrent.component.css']
})
export class CreateConcurrentComponent   {
  name = new FormControl('', Validators.required);
  config = new MatSnackBarConfig();
  constructor(private concurrentRestService : ConcurrentRestServiceService, private snackBar: MatSnackBar) {
    this.setConfig();
  }
  setConfig(){
    let horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    let verticalPosition: MatSnackBarVerticalPosition = 'top';
    let autoHide: number = 2000;
    this.config.verticalPosition = verticalPosition;
    this.config.horizontalPosition = horizontalPosition
    this.config.duration = autoHide;
    this.config.panelClass = ['snackBar'] ;
  }
  getErrorMessage() {
    return this.name.hasError('required') ? 'Campo obrigatório' : '';
  }
  save(){
    if(!this.name.errors){
      let concorrente : Concurrent = new Concurrent();
      concorrente.name = this.name.value;
      
      this.concurrentRestService.registerConcurrent(concorrente).subscribe(
         data => {
            this.snackBar.open( "Concorrente cadastrado com sucesso!", "x", this.config);
            this.name.reset();
          return;
        },
        error => {
          this.snackBar.open( "Falha ao cadastrar concorrente.", "x", this.config);
          return;
        }
      );
    
    }
  }

}
