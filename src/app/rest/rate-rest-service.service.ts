  import { Injectable } from '@angular/core';
  import { HttpClient } from '@angular/common/http';
  import { Rate } from '../model/rate';
  
  @Injectable({
    providedIn: 'root'
  })
  export class RateRestServiceService {
  
    constructor(private http: HttpClient) { }
    registerRate(rate: Rate){
       return this.http.post(`http://localhost:8080/Simulator/rate/createRate`,rate);
    }
  
  }
  