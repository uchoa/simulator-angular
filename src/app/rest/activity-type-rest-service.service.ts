import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivityType } from '../model/activity-type';

@Injectable({
  providedIn: 'root'
})
export class ActivityTypeRestServiceService {

  constructor(private http: HttpClient) { }
  registerActivityType(activityType : ActivityType){
    return this.http.post(`http://localhost:8080/Simulator/activityType/createActivityType`,activityType);
  
}
  getAllActivityType(){
    return this.http.get<any[]>(`http://localhost:8080/Simulator/activityType/getAllActivityType`);
  }
}
