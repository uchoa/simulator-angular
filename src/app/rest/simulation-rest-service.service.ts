import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Simulation } from '../model/simulation';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SimulationRestServiceService {

  constructor(private http: HttpClient) { }
  registerSimulation(simulation : Simulation){
    return this.http.post(`http://localhost:8080/Simulator/simulation/saveSimulation`,simulation);
  }
  finishSimulation(simulation : Simulation){
    return this.http.post(`http://localhost:8080/Simulator/simulation/finishSimulation`,simulation);
  }
  downloadSimulations() {
    const options = { responseType: 'blob' as 'json' }
    return this.http.get<Blob>(`http://localhost:8080/Simulator/simulation/downloadAcceptedSimulation`,options);
  }
}


