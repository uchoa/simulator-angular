import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Concurrent } from '../model/concurrent';

@Injectable({
  providedIn: 'root'
})
export class ConcurrentRestServiceService {

  constructor(private http: HttpClient) { }
  registerConcurrent(concurrent: Concurrent){
    return this.http.post(`http://localhost:8080/Simulator/concurrent/createConcurrent`,concurrent);
  
}
  getAllConcurrent(){
    return this.http.get<any[]>(`http://localhost:8080/Simulator/concurrent/getAllConcurrent`);
  }
}
