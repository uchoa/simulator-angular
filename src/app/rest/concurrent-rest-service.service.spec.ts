import { TestBed } from '@angular/core/testing';

import { ConcurrentRestServiceService } from './concurrent-rest-service.service';

describe('ConcurrentRestServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConcurrentRestServiceService = TestBed.get(ConcurrentRestServiceService);
    expect(service).toBeTruthy();
  });
});
