import { TestBed } from '@angular/core/testing';

import { SimulationRestServiceService } from './simulation-rest-service.service';

describe('SimulationRestServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SimulationRestServiceService = TestBed.get(SimulationRestServiceService);
    expect(service).toBeTruthy();
  });
});
