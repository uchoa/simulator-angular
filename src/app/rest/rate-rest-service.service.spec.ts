import { TestBed } from '@angular/core/testing';

import { RateRestServiceService } from './rate-rest-service.service';

describe('RateRestServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RateRestServiceService = TestBed.get(RateRestServiceService);
    expect(service).toBeTruthy();
  });
});
