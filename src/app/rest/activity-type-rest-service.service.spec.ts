import { TestBed } from '@angular/core/testing';

import { ActivityTypeRestServiceService } from './activity-type-rest-service.service';

describe('ActivityTypeRestServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityTypeRestServiceService = TestBed.get(ActivityTypeRestServiceService);
    expect(service).toBeTruthy();
  });
});
