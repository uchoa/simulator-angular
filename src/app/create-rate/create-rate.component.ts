import { Component } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ActivityTypeRestServiceService} from '../rest/activity-type-rest-service.service'
import {RateRestServiceService} from '../rest/rate-rest-service.service'
import {MatSnackBar, MatSnackBarConfig,  MatSnackBarHorizontalPosition,  MatSnackBarVerticalPosition} from '@angular/material';
import { ActivityType } from '../model/activity-type';
import { Rate } from '../model/rate';
@Component({
  selector: 'app-create-rate',
  templateUrl: './create-rate.component.html',
  styleUrls: ['./create-rate.component.css']
})
export class CreateRateComponent  {
  activityTypes : ActivityType[];
  value = new FormControl('', Validators.required);
  rateType = new FormControl('', Validators.required);
  activityType = new FormControl('', Validators.required);
  config = new MatSnackBarConfig();

  constructor(private activityTypeRestServiceService : ActivityTypeRestServiceService,private rateRestServiceService : RateRestServiceService, private snackBar: MatSnackBar) {
    this.setConfig();
    this.getActivityList();
  }
  getErrorValueMessage() {
    return this.value.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorRateTypeMessage() {
    return this.rateType.hasError('required') ? 'Campo obrigatório' : '';
  }
  getErrorActivityTypeMessage() {
    return this.activityType.hasError('required') ? 'Campo obrigatório' : '';
  }
  setConfig(){
    let horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    let verticalPosition: MatSnackBarVerticalPosition = 'top';
    let autoHide: number = 2000;
    this.config.verticalPosition = verticalPosition;
    this.config.horizontalPosition = horizontalPosition
    this.config.duration = autoHide;
    this.config.panelClass = ['snackBar'] ;
  }
  getActivityList(){
     this.activityTypeRestServiceService.getAllActivityType().subscribe(
      data => {
        this.activityTypes = data;
      return;
    },
    error => {
      this.snackBar.open( "Falha ao carregar lista de ramo de atividade.", "x", this.config);
      return;
    }
    );

  }
  save(){
    if(!this.activityType.errors){
      let rate : Rate = new Rate();
      rate.activityType= this.activityType.value;
      rate.rateType = this.rateType.value;
      rate.rate = this.value.value;
      
      this.rateRestServiceService.registerRate(rate).subscribe(
         data => {
            this.snackBar.open( "Taxa cadastrado com sucesso!", "X",this.config);
            this.activityType.reset();
            this.value.reset();
            this.rateType.reset();
          return;
        },
        error => {
          this.snackBar.open( "Falha ao cadastrar taxa.", "", this.config);
          return;
        }
      );
    }
  }

}
